// pages/buycard/buycard.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    telphonum: "12312345678",
    rules: [],
    selected_id: '',
    selected_price: '',
    save: '',
    total_to_pay_money: '',
    order_id: '',
  },

  know: function() {
    wx.showModal({
      title: '购卡须知',
      showCancel: false,
      content: this.data.save.vip_protocol,
      success: function(res) {}
    })
  },

  buy: function() {
    var that = this

    if (that.data.selected_id == '' || that.data.selected_id == undefined) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择一种会员卡！',
        success: function(res) {}
      })
    } else {
      //伪造的商品数据
      var goods_arr_tmp = [{
        goods_type: 9,
        goods_id: that.data.selected_id,
        goods_name: that.data.save.vip_name,
        photo_url: '',
        unit_price: that.data.selected_price,
        original_price: that.data.selected_price,
        discount: 0,
        quantity: 1,
        note: "购买权益卡",
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id
      }]
      console.log("goods_arr", goods_arr_tmp)
      //创建订单，获得待支付总金额
      wx.request({
        url: getApp().globalData.server + '/API/Shopping/do_order',
        data: {
          shop_id: getApp().globalData.settings.shop_id,
          shop_name: getApp().globalData.settings.shop_name,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
          username: getApp().globalData.userInfo_detail.username,
          note: "购买权益卡",
          order_type: 9,
          goods_arr: JSON.stringify(goods_arr_tmp),
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log('创建订单，获得待支付总金额', res.data)
          if (res.data.error_no != 0) {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          } else if (res.data.error_no == 0) {
            that.data.total_to_pay_money = res.data.data.total_to_pay_money
            console.log('需支付金额（total_to_pay_money）：', that.data.total_to_pay_money)
            that.data.order_id = res.data.data.order_id
            console.log('订单号（order_id）：', that.data.order_id)
            //微信支付
            wx.login({
              success: function(res) {
                if (res.code) {
                  console.log('code', res.code, "user_id", getApp().globalData.userInfo_detail.user_id)
                  wx.request({
                    url: getApp().globalData.server + "/API/Pay/pay_request",
                    data: {
                      code: res.code,
                      company_id: getApp().globalData.settings.company_id,
                      shop_id: getApp().globalData.settings.shop_id,
                      order_id: that.data.order_id,
                      total_to_pay_money: that.data.total_to_pay_money,
                      user_id: getApp().globalData.userInfo_detail.user_id
                    },
                    method: "POST",
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    success: function(res2) {
                      console.log(res2)
                      if (res2.data.error_no != 0) {
                        wx.showModal({
                          title: '哎呀',
                          content: '请求支付失败！',
                          success: function(res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      } else if (res2.data.error_no == 0) {
                        wx.requestPayment({
                          'timeStamp': res2.data.data.timeStamp + '',
                          'nonceStr': res2.data.data.nonceStr,
                          'package': res2.data.data.package,
                          'signType': 'MD5',
                          'paySign': res2.data.data.paySign,
                          'success': function(res) {
                            wx.showModal({
                              title: '恭喜',
                              showCancel: false,
                              content: '支付成功！',
                              success: function(res) {
                                wx.reLaunch({
                                  url: '../load/load'
                                })
                              }
                            })
                          },
                          'fail': function(res) {
                            wx.showModal({
                              title: '提示',
                              showCancel: false,
                              content: '支付失败，请刷新后重试！',
                              success: function(res) {}
                            })
                          },
                          'complete': function(res) {}
                        })
                      }
                    },
                    fail: function(res) {
                      wx.showModal({
                        title: '哎呀～',
                        content: '请求支付失败！',
                        success: function(res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                  })
                } else {
                  console.log('登录失败！' + res.errMsg)
                  wx.showModal({
                    title: '哎呀～',
                    content: '内部错误1',
                    success: function(res) {
                      if (res.confirm) {
                        console.log('用户点击确定')
                      } else if (res.cancel) {
                        console.log('用户点击取消')
                      }
                    }
                  })
                }
              }
            });
          }
        },
        fail: function(res) {
          wx.showModal({
            title: '哎呀～',
            content: '网络不在状态呢！',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
    }
  },

  radioChange: function(e) {
    //console.log(e)
    console.log('radio发生change事件，携带value值为：', e.target.dataset)
    this.setData({
      selected_id: e.target.dataset.id,
      selected_price: e.target.dataset.price,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this

    this.setData({
      telphonum: getApp().globalData.settings.phone,
    })
    //得到权益卡所有售卖规则
    wx.request({
      url: getApp().globalData.server + '/API/Vip/get_all_vip_sell_rules',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('得到权益卡所有售卖规则', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            rules: res.data.data.sell_rules,
            save: res.data.data,
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  }
})