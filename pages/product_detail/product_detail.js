// pages/product_detail/product_detail.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_use_payment: '1',
    goodid: '',
    gooddetail: [],
    detailHeight: 0,
    buttonHeight: 0,
    windowHeight: getApp().globalData.windowHeight,
    windowWidth: getApp().globalData.windowWidth,
    comfirm_setting: {},
    delivery: "运费：？元",
    num: '数量：？个',

    //toast
    chooseSize: false,
    animationData: {},
    quatity: 0,
    // input默认是1 
    selectnum: 1,
    // 使用data数据对象设置样式名 
    plusStatus: 'disabled',
    minusStatus: 'disabled',
  },

  back: function() {
    wx.navigateBack({
      delta: 1,
    })
  },

  buy: function() {
    var that = this
    if (that.data.quatity > 0) {
      wx.showModal({
        title: '提示',
        content: '确定要加入购物车么？',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
            var carts = getApp().globalData.yi_shopinglist

            var tip = 0
            for (var i = 0; i < carts.length; i++) {
              if (carts[i].goodid == that.data.goodid) {
                getApp().globalData.yi_shopinglist[i].numb += that.data.selectnum
                tip = 1
              }
            }
            if (tip == 0) {
              var temp = {
                goodid: that.data.goodid,
                gooddetail: that.data.gooddetail,
                numb: that.data.selectnum,
                selected: false,
              }
              getApp().globalData.yi_shopinglist.push(temp)
            }
            console.log('当前购物车内容：', getApp().globalData.yi_shopinglist)
            wx.redirectTo({
              url: '../cart/cart'
            })
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      wx.showModal({
        title: 'Sorry',
        showCancel: false,
        content: '您选择的商品已经售空，请下次早点来哦~',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },

  /* 点击减号 */
  bindMinus: function() {
    if (this.data.quatity > 1) {
      var selectnum = this.data.selectnum;
      // 如果大于1时，才可以减 
      if (selectnum > 1) {
        selectnum--;
      }
      // 只有大于一件的时候，才能normal状态，否则disable状态 
      var minusStatus = selectnum <= 1 ? 'disabled' : 'normal';
      var plusStatus = selectnum > this.data.quatity ? 'disabled' : 'normal';
      // 将数值与状态写回 
      this.setData({
        selectnum: selectnum,
        minusStatus: minusStatus,
        plusStatus: plusStatus,
      });
    }
  },
  /* 点击加号 */
  bindPlus: function() {
    if (this.data.quatity > 1) {
      var selectnum = this.data.selectnum;
      // 如果小于最大值才可以加 
      if (selectnum < this.data.quatity) {
        selectnum++;
      }
      // 只有大于一件的时候，才能normal状态，否则disable状态 
      var minusStatus = selectnum < 1 ? 'disabled' : 'normal';
      var plusStatus = selectnum >= this.data.quatity ? 'disabled' : 'normal';
      // 将数值与状态写回 
      this.setData({
        selectnum: selectnum,
        minusStatus: minusStatus,
        plusStatus: plusStatus,
      });
    }
  },
  /* 输入框事件 */
  bindManual: function(e) {
    var selectnum = e.detail.value;
    // 将数值与状态写回 
    this.setData({
      selectnum: selectnum
    });
  },

  wonder: function(e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },

  util: function(currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200, //动画时长  
      timingFunction: "linear", //线性  
      delay: 0 //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停  
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function() {
      // 执行第二组动画：Y轴不偏移，停  
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭抽屉  
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false
        });
      }
    }.bind(this), 200)

    // 显示抽屉  
    if (currentStatu == "open") {
      this.setData({
        showModalStatus: true
      });
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)

    var that = this

    that.setData({
      goodid: options.id,
      detailHeight: that.data.windowHeight * 0.92,
      buttonHeight: that.data.windowHeight * 0.08,
      comfirm_setting: getApp().globalData.settings,
    })

    //得到指定的商品详情
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_good_detail_by_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        good_id: options.id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('得到指定的商品详情', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          console.log(res.data.data.good_detail)
          that.setData({
            gooddetail: res.data.data.good_detail
          })
          //运费
          if (res.data.data.good_detail.delivery_type == '0') {
            that.setData({
              delivery: '无需快递'
            })
          } else if (res.data.data.good_detail.delivery_type == '1') {
            that.setData({
              delivery: '运费：0.00元（包邮）'
            })
          } else if (res.data.data.good_detail.delivery_type == '2') {
            that.setData({
              delivery: '运费：' + res.data.data.good_detail.delivery_price + '元'
            })
          } else if (res.data.data.good_detail.delivery_type == '3') {
            that.setData({
              delivery: '到店自取'
            })
          }
          //数量
          if (res.data.data.good_detail.total_num == '-1') {
            that.data.quatity = 999999
            that.setData({
              num: '数量不限',
              quatity: 999999,
            })
          } else {
            that.data.quatity = (parseInt(res.data.data.good_detail.total_num) - parseInt(res.data.data.good_detail.has_been_bought_num))
            that.setData({
              num: '数量：' + (parseInt(res.data.data.good_detail.total_num) - parseInt(res.data.data.good_detail.has_been_bought_num)) + '个',
              quatity: parseInt(res.data.data.good_detail.total_num) - parseInt(res.data.data.good_detail.has_been_bought_num),
            })
          }
          if (that.data.quatity > 1) {
            that.setData({
              plusStatus: 'normal',
            })
          } else if (that.data.quatity <= 0) {
            that.setData({
              selectnum: 0,
            })
          }
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    // if (this.data.quatity > 1) {
    //   this.setData({
    //     plusStatus: 'normal',
    //   })
    // } else if (this.data.quatity <= 0) {
    //   this.setData({
    //     selectnum: 0,
    //   })
    // }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  }
})