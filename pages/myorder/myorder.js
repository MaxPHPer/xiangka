// pages/myorder/myorder.js
var util = require('../../utils/util.js');
var timestamp_to_minite = require('../../utils/util.js').timestamp_to_minite;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    currentTab: 0,

    //订单数据
    order_all: [],
    order_nopay: [],
    order_yespay: [],
    del_set: [],
    records: [],
    no_pay_order: [],

    //附加信息
    // y_help: [
    //   leixing = [//订单类型
    //     txt = '',//文字
    //     col = '',//颜色
    //   ],
    //   dingdan = [//订单状态
    //     txt = '',
    //     col = '',
    //   ],
    //   zhifu = [//支付状态
    //     txt = '',
    //     col = '',
    //   ],
    //   yue_money = '',//余额支付金额
    //   zhifu_yime = '',//支付时间
    //   xiadan_time = '',//下单时间
    // ],


    to_be_used: [{
      "id": "60",
      "name": "测试用的",
      "img_url": "http://www.joymem.com/Public/API/image/item.png",
      "exchange_cost_points": "8",
      "exchange_cost_money": "7",
      "category_name": "d级"
    }],
    receive_disabled: 0,
    status: "去使用"

  },

  receive: function (e) {
      var that = this
      var index = e.currentTarget.dataset.index

      wx.navigateTo({
        url: '../receive/receive?order_id=' + that.data.to_be_used[index].order_detail[0].order_id,
      })
  },


  back: function () {
    wx.reLaunch({
      url: '../index/index',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;

    //console.log(options)
    if (options.tab) {
      that.setData({
        currentTab: options.tab,
        shop: getApp().globalData.settings.company_name + getApp().globalData.settings.shop_name,
        source: "积分商城兑换"
      })
    }

    /** 
     * 获取系统信息 
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });

    //得到所有订单
    wx.request({
      url: getApp().globalData.server + '/API/Shopping/get_one_user_all_orders',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        is_need_order_details: 1,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('得到所有订单', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          that.setData({
            order_all: that.Convert(res.data.data).orders,
          })
          //console.log(that.data.order_all)
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })

    that.get_unused_orders()
  },

  get_unused_orders: function () {
    var that = this

    //得到所有待使用的订单
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_all_unused_goods_order',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('得到所有待使用的订单', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          that.setData({
            to_be_used: res.data.data.orders,
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
  },

  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })

      if (e.target.dataset.current == 2) {
        wx.request({
          url: getApp().globalData.server + '/API/Shopping/get_one_user_all_orders',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
            pay_status: 1,
            is_need_order_details: 1,
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('得到所有订单', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              that.setData({
                no_pay_order: that.Convert(res.data.data).orders,
              })
              //console.log(that.data.no_pay_order)
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      }
      else if (e.target.dataset.current == 3) {
        //获得消费记录
        wx.request({
          url: getApp().globalData.server + '/API/Shopping/get_one_user_all_package_records',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('得到所有消费记录', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              var records_temp = res.data.data.records
              for (var i = 0; i < records_temp.length; i++) {
                records_temp[i].consume_times = parseInt(records_temp[i].consume_times)
                records_temp[i].consume_timestamp = timestamp_to_minite(parseInt(records_temp[i].consume_timestamp))
              }

              that.setData({
                records: records_temp
              })
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      } 
      else if (e.target.dataset.current == 1) {
        that.get_unused_orders()
      }

    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this

    that.get_unused_orders()

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  },

  Convert: function (data) {
    //console.log(data)
    var that = this
    for (var i = 0; i < data.orders.length; i++) {
      var y_help = {
        leixing: that.ConvertOrdertype(data.orders[i].type),
        dingdan: that.ConvertOrderstatus(data.orders[i].order_status),
        zhifu: that.ConvertPaystatus(data.orders[i].pay_status),
        zhifu_time: that.timestampToTime(data.orders[i].pay_timestamp),
        xiadan_time: that.timestampToTime(data.orders[i].create_timestamp),
        yue_money: parseFloat(data.orders[i].main_balance_pay_money) + parseFloat(data.orders[i].second_balance_pay_money)
      }
      //console.log(y_help)
      data.orders[i].y_help = y_help
    }
    return data;
    
  },

  ConvertPaystatus: function (n) {
    //支付状态：0：不需要支付，1未支付，2微信支付，3余额支付
    var tmp = {
      txt: '',
      col: '',
    }
    switch (n) {
      case '0':
        tmp.txt = '不需要支付';
        tmp.col = '#01c00b';
        break;
      case '1':
        tmp.txt = '未支付';
        tmp.col = '#d81e06';
        break;
      case '2':
        tmp.txt = '微信支付';
        tmp.col = '#f6ef37';
        break;
      case '3':
        tmp.txt = '余额支付';
        tmp.col = '#17abe3';
        break;
      default:
        tmp.txt = '未知';
        tmp.col = '#707070';
    }
    return tmp;
  },

  ConvertOrderstatus: function (n) {
    //订单状态：0 : 审核中，1 : 已批准，2 : 未通过，3 : 已取消，4 : 退款中，5 ：退款完成，6 : 退款被拒绝
    var tmp = {
      txt: '',
      col: '',
    }
    switch (n) {
      case '0':
        tmp.txt = '审核中';
        tmp.col = '#f6ef37';
        break;
      case '1':
        tmp.txt = '已批准';
        tmp.col = '#01c00b';
        break;
      case '2':
        tmp.txt = '未通过';
        tmp.col = '#fc1c18';
        break;
      case '3':
        tmp.txt = '已取消';
        tmp.col = '#e89abe';
        break;
      case '4':
        tmp.txt = '退款中';
        tmp.col = '#a33af9';
        break;
      case '5':
        tmp.txt = '退款完成';
        tmp.col = '#07e8d0';
        break;
      case '6':
        tmp.txt = '退款被拒绝';
        tmp.col = '#e87907';
        break;
      default:
        tmp.txt = '未知';
        tmp.col = '#707070';
    }
    return tmp;
  },

  ConvertOrdertype: function (n) {
    //1表示商品购买，2表示套餐购买，3表示储值，4表示折扣卡，5表示积分兑换，6表示在线买单，7表示抽奖兑换
    var tmp = {
      txt: '',
      col: '',
    }
    switch (n) {
      case '1':
        tmp.txt = '商品购买';
        tmp.col = '#fa5902';
        break;
      case '2':
        tmp.txt = '套餐购买';
        tmp.col = '#17abe3';
        break;
      case '3':
        tmp.txt = '储值';
        tmp.col = '#7cba59';
        break;
      case '4':
        tmp.txt = '折扣卡';
        tmp.col = '#db639b';
        break;
      case '5':
        tmp.txt = '积分兑换';
        tmp.col = '#dc0398';
        break;
      case '6':
        tmp.txt = '在线买单';
        tmp.col = '#840aca';
        break;
      case '7':
        tmp.txt = '抽奖兑换';
        tmp.col = '#41ff93';
        break;
      default:
        tmp.txt = '未知';
        tmp.col = '#707070';
    }
    return tmp;
  },

  timestampToTime: function (timestamp) {
    //console.log(timestamp)
    if (timestamp == '0') {
      return '无效时间'
    }
    else {
      var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
      var Y = date.getFullYear() + '-';
      var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
      var D = date.getDate() + ' ';
      var h = date.getHours() + ':';
      var m = date.getMinutes() + ':';
      var s = date.getSeconds();
      return Y + M + D + h + m + s;
    }
  }
})