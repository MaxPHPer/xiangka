// pages/good_detail/good_detail.js

var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    good: {},
    id: 0,
    quantity: 1,
    remainder: -1
  },

  formSubmit: function(e) {
    var that = this
    var good = that.data.good

    wx.showModal({
      title: '提示！',
      content: '是否确认兑换？此次兑换将会扣除' + that.data.good.exchange_cost_points * that.data.quantity + "积分，还需额外支付" + good.exchange_cost_money * that.data.quantity + "元",
      success: function(res) {
        if (res.confirm) {
          console.log('用户点击确定')

          var goods_arr_temp = [{
            goods_type: good.goods_type,
            goods_id: good.id,
            goods_name: good.name,
            photo_url: good.img_url,
            unit_price: good.exchange_cost_money,
            original_price: good.original_price,
            discount: good.discount,
            quantity: that.data.quantity,
            note: good.category_name,
            company_id: getApp().globalData.settings.company_id,
            shop_id: getApp().globalData.settings.shop_id
          }]

          //创建积分兑换订单
          wx.request({
            url: getApp().globalData.server + "/API/Goods/do_exchange_by_points",
            data: {
              company_id: getApp().globalData.settings.company_id,
              shop_id: getApp().globalData.settings.shop_id,
              goods_id: that.data.id,
              shop_name: getApp().globalData.settings.shop_name,
              user_id: getApp().globalData.userInfo_detail.id,
              username: getApp().globalData.userInfo_detail.username,
              note: "积分兑换",
              order_type: 1,
              to_pay_points: that.data.good.exchange_cost_points * that.data.quantity,
              receiver: {
                receiver_username: "",
                receiver_addr: "",
                receiver_phone: ""
              },
              goods_arr: JSON.stringify(goods_arr_temp)
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function(res2) {
              console.log(res2)
              if (res2.data.error_no != 0) {
                wx.showModal({
                  title: '哎呀',
                  content: '创建积分兑换订单失败',
                  success: function(res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              } else if (res2.data.error_no == 0) {
                getApp().globalData.userInfo_detail.point = res2.data.data.current_point

                console.log('积分变动微信提醒(formId)：', e.detail.formId)
                //积分变动微信提醒
                wx.request({
                  url: getApp().globalData.server + '/API/Point/point_wechat_notify',
                  data: {
                    company_id: getApp().globalData.settings.company_id,
                    shop_id: getApp().globalData.settings.shop_id,
                    user_id: getApp().globalData.userInfo_detail.user_id,
                    username: getApp().globalData.userInfo_detail.username,
                    openid: getApp().globalData.open_id,
                    form_id: e.detail.formId,
                    points_type: '扣除',
                    points_from: '积分商城',
                    consume_points: that.data.good.exchange_cost_points * that.data.quantity,
                    total_points: res2.data.data.current_point,
                  },
                  method: "POST",
                  header: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  success: function(res3) {
                    console.log('积分变动微信提醒', res3.data)
                    if (res3.data.error_no != 0) {
                      // wx.showModal({
                      //   title: '哎呀～',
                      //   content: '出错了呢！' + res3.data.data.error_msg,
                      //   success: function (res3) {
                      //     if (res3.confirm) {
                      //       console.log('用户点击确定')
                      //     } else if (res3.cancel) {
                      //       console.log('用户点击取消')
                      //     }
                      //   }
                      // })
                    } else if (res3.data.error_no == 0) {}
                  },
                  fail: function(res) {
                    // wx.showModal({
                    //   title: '哎呀～',
                    //   content: '网络不在状态呢！',
                    //   success: function (res) {
                    //     if (res.confirm) {
                    //       console.log('用户点击确定')
                    //     } else if (res.cancel) {
                    //       console.log('用户点击取消')
                    //     }
                    //   }
                    // })
                  }
                })

                var money = res2.data.data.total_to_pay_money
                if (money > 0) {
                  wx.reLaunch({
                    url: '../shop_money/shop_money?good=' + JSON.stringify(that.data.good) + "&source=10" + "&quantity=" + that.data.quantity + "&order_id=" + res2.data.data.order_id,
                  })
                } else if (money == 0) {
                  wx.redirectTo({
                    url: '../myorder/myorder?tab=' + 1,
                  })
                }

              }
            },
            fail: function(res) {
              wx.showModal({
                title: '哎呀～',
                content: '创建积分兑换订单失败',
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })

        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this

    that.setData({
      id: options.goods_id
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this

    //获取积分商城商品详情
    wx.request({
      url: getApp().globalData.server + "/API/Goods/get_one_allow_exchange_goods",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
        goods_id: that.data.id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res2) {
        console.log(res2)
        if (res2.data.error_no != 0) {
          wx.showModal({
            title: '哎呀',
            content: '获取积分商城商品详情失败',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res2.data.error_no == 0) {
          that.setData({
            good: res2.data.data,
            remainder: parseInt(res2.data.data.total_num_for_exchange) - parseInt(res2.data.data.has_been_bought_num_for_exchange)
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取积分商城商品详情失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  }
})