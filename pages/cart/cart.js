// pages/cart/cart.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isshowdel: false,
    selectedAllStatus: false,
    loadingHidden: true,
    total: 0.00,
    carts: [],
    yunfee: '运费：￥0.00',
    delivery: [],
  },

  togood: function() {
    wx.navigateBack({
      delta: 1
    })
  },

  wantbuy: function() {
    var that = this

    var geshu = 0;
    var carts = this.data.carts;
    var pay = [];
    for (var i = 0; i < carts.length; i++) {
      if (carts[i].selected == true) {
        geshu++;
        pay.push(carts[i])
      }
    }
    if (geshu == 0) {
      wx.showModal({
        title: '提示',
        content: '请选中您要购买的物品',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      if (that.data.yunfee == '运费正在计算中...') {
        wx.showModal({
          title: '提示',
          content: '正在计算运费中，请稍候...',
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      } else {
        getApp().globalData.yi_buylist.goods = pay;
        getApp().globalData.yi_buylist.send = that.data.delivery;
        getApp().globalData.yi_buylist.allm = (parseFloat(that.data.total) + parseFloat(that.data.delivery.total_delivery_fee)).toFixed(2);
        wx.navigateTo({
          url: '/pages/buygoods/buygoods'
        })
      }
    }

    // wx.showModal({
    //   title: '提示',
    //   content: '请联系店家购买~',
    //   showCancel: false,
    //   success: function(res) {
    //     if (res.confirm) {
    //       console.log('用户点击确定')
    //     } else if (res.cancel) {
    //       console.log('用户点击取消')
    //     }
    //   }
    // })
  },

  toedit: function() {
    this.setData({
      isshowdel: ~this.data.isshowdel
    })
  },

  // loadingChange: function () {
  //   this.setData({
  //     loadingHidden: true
  //   });
  // },

  bindSelectAll: function() {
    // 环境中目前已选状态
    var selectedAllStatus = this.data.selectedAllStatus;
    // 取反操作
    selectedAllStatus = !selectedAllStatus;
    // 购物车数据，关键是处理selected值
    var carts = this.data.carts;
    if (carts.length > 0) {
      for (var i = 0; i < carts.length; i++) {
        carts[i].selected = selectedAllStatus;
      }
    } else {
      selectedAllStatus = false
    }
    // 遍历
    this.setData({
      selectedAllStatus: selectedAllStatus,
      carts: carts
    });
    this.sum();
  },

  sum: function() {
    var that = this

    var carts = this.data.carts;
    // 计算总金额
    var total = 0.00;
    for (var i = 0; i < carts.length; i++) {
      if (carts[i].selected) {
        total += parseInt(carts[i].numb) * parseFloat(carts[i].gooddetail.unit_price).toFixed(2);
      }
    }
    // 写回经点击修改后的数组
    this.setData({
      carts: carts,
      loadingHidden: true,
      total: parseFloat(total).toFixed(2),
      yunfee: '运费正在计算中...',
      loadingHidden: false,
    });

    var ids = []
    for (var i = 0; i < carts.length; i++) {
      if (carts[i].selected) {
        ids.push(carts[i].goodid)
      }
    }
    // var data = {
    //   shop_id: getApp().globalData.settings.shop_id,
    //   company_id: getApp().globalData.settings.company_id,
    //   goods_ids: ids,
    // }
    // console.log(data)
    console.log('商品id数组', ids)
    //根据商品id数组获取运费
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_delivery_fee_by_goods_ids',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        goods_ids: ids,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('根据商品id数组获取运费', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            yunfee: "运费： ￥" + res.data.data.total_delivery_fee.toFixed(2),
            delivery: res.data.data,
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      },
      complete: function() {
        that.setData({
          loadingHidden: true,
        })
      }
    })
  },

  bindCheckbox: function(e) {
    /*绑定点击事件，将checkbox样式改变为选中与非选中*/
    //拿到下标值，以在carts作遍历指示用
    var index = parseInt(e.currentTarget.dataset.index);
    //原始的icon状态
    var selected = this.data.carts[index].selected;
    var carts = this.data.carts;
    // 对勾选状态取反
    carts[index].selected = !selected;
    var selectArr = [];
    // 遍历
    for (var i = 0; i < carts.length; i++) {
      if (carts[i].selected == true) {
        selectArr.push(carts[i].selected)
      }
    }
    if (e.currentTarget.dataset.type == 'success_circle') {
      this.setData({
        selectedAllStatus: false
      });
    }
    if (selectArr.length == carts.length) {
      this.setData({
        selectedAllStatus: true
      });
    }
    var carts = this.data.carts;
    // 写回经点击修改后的数组
    this.setData({
      carts: carts,
    });
    this.sum();
  },

  del: function(e) {
    //删除购物车商品商品
    var carts = this.data.carts;
    var index = e.target.dataset.index;
    carts.splice(index, 1);
    this.sum();
    this.setData({
      carts: carts
    });
    var temp = carts
    for (var i = 0; i < temp.length; i++) {
      temp[i].selected = false
    }
    console.log('删除后的商品列表', temp)
    getApp().globalData.yi_shopinglist = temp
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      carts: getApp().globalData.yi_shopinglist
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      carts: getApp().globalData.yi_shopinglist
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      carts: getApp().globalData.yi_shopinglist,
      selectedAllStatus: false,
      total: parseFloat(this.data.total).toFixed(2),
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})