
var pay = (res2, that, route, need_to_pay_wechat_money = 0) => {
  if (route == 1) {
    wx.requestPayment(
      {
        'timeStamp': res2.data.data.timeStamp + '',
        'nonceStr': res2.data.data.nonceStr,
        'package': res2.data.data.package,
        'signType': 'MD5',
        'paySign': res2.data.data.paySign,
        'success': function (res) {
          console.log(res)
          if (res.errMsg == "requestPayment:ok") {

            wx.navigateTo({
              url: '../pay_complete/pay_complete?price=' + that.data.thing.actual_price,
            })

          } else {
            wx.showModal({
              title: '哎呀',
              content: '支付失败！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        },
        'fail': function (res) {
          console.log(res)
          wx.showModal({
            title: '哎呀',
            content: '支付失败！',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        },
        'complete': function (ßres) { }
      }
    )
  }
  else if (route == 3) {
    wx.requestPayment(
      {
        'timeStamp': res2.data.data.timeStamp + '',
        'nonceStr': res2.data.data.nonceStr,
        'package': res2.data.data.package,
        'signType': 'MD5',
        'paySign': res2.data.data.paySign,
        'success': function (res) {
          console.log(res)
          if (res.errMsg == "requestPayment:ok") {

            wx.navigateTo({
              url: '../pay_complete/pay_complete?price=' + need_to_pay_wechat_money + "&source=3" ,
            })

          } else {
            wx.showModal({
              title: '哎呀',
              content: '支付失败！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        },
        'fail': function (res) {
          console.log(res)
          wx.showModal({
            title: '哎呀',
            content: '支付失败！',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        },
        'complete': function (ßres) { }
      }
    )
  }
  else if (route == 2) {
    wx.requestPayment(
      {
        'timeStamp': res2.data.data.timeStamp + '',
        'nonceStr': res2.data.data.nonceStr,
        'package': res2.data.data.package,
        'signType': 'MD5',
        'paySign': res2.data.data.paySign,
        'success': function (res) {
          console.log(res)
          wx.showModal({
            title: '恭喜',
            showCancel: false,
            content: '支付成功！',
            success: function (res) {

            },
            complete: function (res) {
              wx.reLaunch({
                url: '../myorder/myorder'
              })
            }
          })
        },
        'fail': function (res) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '支付失败，请刷新后重试！',
            success: function (res) { }
          })
        },
        'complete': function (res) { }
      }
    )
  }
  else if (route == 4) {//在线买单
    wx.requestPayment(
      {
        'timeStamp': res2.data.data.timeStamp + '',
        'nonceStr': res2.data.data.nonceStr,
        'package': res2.data.data.package,
        'signType': 'MD5',
        'paySign': res2.data.data.paySign,
        'success': function (res) {
          console.log(res)
          wx.showModal({
            title: '恭喜',
            showCancel: false,
            content: '支付成功！',
            success: function (res) {

            },
            complete: function (res) {
              wx.reLaunch({
                url: '../myorder/myorder'
              })
            }
          })
        },
        'fail': function (res) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '支付失败，请刷新后重试！',
            success: function (res) {
              
             },
            complete: function (res) {
              wx.reLaunch({
                url: '../myorder/myorder'
              })
            }
          })
        },
        'complete': function (res) { }
      }
    )
  }
}

module.exports = {
  pay: pay
}
