// pages/enroll/enroll.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    telphonum: "12312345678",
    currentdate: "2018-3-13",
    birthdate: '2018-3-13',
    phonumber: "",
    logincode: "",
    userInfo: [],
    location: "",
    name: "",
    gender: "1",
    password: "",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      telphonum: getApp().globalData.settings.phone,
      userInfo: getApp().globalData.userInfo,
    })
    var time = util.currentDate(new Date());
    this.setData({
      currentdate: time,
      birthdate: time
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  bindDateChange: function(e) {
    this.setData({
      birthdate: e.detail.value
    })
  },

  getPhoneNumber: function(e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.iv)
    console.log(e.detail.encryptedData)
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '未授权',
        success: function(res) {}
      })
    } else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '同意授权',
        success: function(res) {}
      })
    }
  },

  formSubmit: function(e) {
    var that = this
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    if (that.data.name == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入姓名！',
        success: function(res) {}
      })
    } else if (that.data.phonumber == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入号码！',
        success: function(res) {}
      })
    } else if (that.data.phonumber.length != 11) {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '手机号长度有误，请重新输入！',
        success: function(res) {}
      })
    } else if (!myreg.test(that.data.phonumber)) {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入正确的手机号码（仅支持中国大陆）！',
        success: function(res) {}
      })
    } else if (that.data.password == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入密码！',
        success: function(res) {}
      })
    } else if (that.data.password.length > 8 || that.data.password.length < 6) {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '密码长度有误，请重新输入！',
        success: function(res) {}
      })
    }
    // else if (that.data.location == '') {
    //   wx.showModal({
    //     title: '提示！',
    //     showCancel: true,
    //     content: '地址还没有输入，确定要注册吗？',
    //     success: function (res) {
    //       if (res.confirm) {
    //         console.log('用户点击确定')
    //         wx.showModal({
    //           title: '恭喜！',
    //           showCancel: false,
    //           content: '注册成功',
    //           success: function (res) { }
    //         })
    //       } else if (res.cancel) {
    //         console.log('用户点击取消')
    //       }
    //     }
    //   })
    // }
    else {
      //console.log(that.data.name, that.data.phonumber, that.data.password, that.data.birthdate, that.data.gender, getApp().globalData.open_id, getApp().globalData.settings.shop_id, getApp().globalData.settings.company_id)
      wx.request({
        url: getApp().globalData.server + '/API/Login/phone_register',
        data: {
          name: that.data.name,
          phone: that.data.phonumber,
          password: that.data.password,
          birthday: that.data.birthdate,
          sex: that.data.gender,
          open_id: getApp().globalData.open_id,
          shop_id: getApp().globalData.settings.shop_id,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log(res.data)
          if (res.data.error_no != 0) {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          } else if (res.data.error_no == 0) {
            getApp().globalData.userInfo_detail = res.data.data.user
            getApp().globalData.userInfo_detail.point = res.data.data.point.current_point
            console.log(getApp().globalData.userInfo_detail)

            console.log('积分变动微信提醒(formId)：', e.detail.formId)
            //积分变动微信提醒
            wx.request({
              url: getApp().globalData.server + '/API/Point/point_wechat_notify',
              data: {
                company_id: getApp().globalData.settings.company_id,
                shop_id: getApp().globalData.settings.shop_id,
                user_id: getApp().globalData.userInfo_detail.user_id,
                username: getApp().globalData.userInfo_detail.username,
                openid: getApp().globalData.open_id,
                form_id: e.detail.formId,
                points_type: '新增',
                points_from: '会员注册',
                consume_points: res.data.data.point.add_point,
                total_points: res.data.data.point.current_point,
              },
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success: function(res) {
                console.log('积分变动微信提醒', res.data)
                if (res.data.error_no != 0) {
                  // wx.showModal({
                  //   title: '哎呀～',
                  //   content: '出错了呢！' + res.data.data.error_msg,
                  //   success: function (res) {
                  //     if (res.confirm) {
                  //       console.log('用户点击确定')
                  //     } else if (res.cancel) {
                  //       console.log('用户点击取消')
                  //     }
                  //   }
                  // })
                } else if (res.data.error_no == 0) {}
              },
              fail: function(res) {
                // wx.showModal({
                //   title: '哎呀～',
                //   content: '网络不在状态呢！',
                //   success: function (res) {
                //     if (res.confirm) {
                //       console.log('用户点击确定')
                //     } else if (res.cancel) {
                //       console.log('用户点击取消')
                //     }
                //   }
                // })
              }
            })

            wx.showModal({
              title: '恭喜！',
              showCancel: false,
              content: '注册成功，积分 +' + res.data.data.point.add_point,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                }
              },
              complete: function(res) {
                wx.reLaunch({
                  url: '../load/load'
                })
              }
            })
          }
        },
        fail: function(res) {
          wx.showModal({
            title: '哎呀～',
            content: '网络不在状态呢！',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
    }
  },

  radioChange: function(e) {
    this.setData({
      gender: e.detail.value
    })
    console.log('radio发生change事件，携带value值为：', e.detail.value)
    //console.log(this.data.gender)
  },

  signin: function() {
    wx.redirectTo({
      url: '/pages/login/login'
    })
  },

  nameInput: function(e) {
    this.data.name = e.detail.value
  },

  phonumberInput: function(e) {
    this.data.phonumber = e.detail.value
  },

  passwordInput: function(e) {
    this.data.password = e.detail.value
  },
})