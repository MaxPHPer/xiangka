// pages/about/about.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_show: true
  },

  saveImgToPhotosAlbumTap: function() {
    var that = this

    wx.authorize({
      scope: 'scope.writePhotosAlbum',
      success() {
        console.log("授权成功！")
      },
      complete() {
        wx.getSetting({
          success(res) {
            if (!res.authSetting['scope.writePhotosAlbum']) {
              wx.showModal({
                title: '提示',
                content: '该功能需要您的授权才可使用，请您同意授权！',
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                    wx.openSetting({
                      success: (res) => {
                        /*
                         * res.authSetting = {
                         *   "scope.userInfo": true,
                         *   "scope.userLocation": true
                         * }
                         */
                        console.log(res)
                      }
                    })
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            } else {
              that.baocun()
            }
          }
        })
      }
    })
  },

  baocun: function() {
    wx.downloadFile({
      url: 'https://www.joymem.com//Public/API/image/shijianfeng_wechat.png',
      success: function(res) {
        console.log(res)
        wx.saveImageToPhotosAlbum({
          filePath: res.tempFilePath,
          success: function(res) {
            console.log('success', res)
            wx.showModal({
              title: '提示',
              content: '恭喜，保存成功！',
              showCancel: false,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          },
          fail: function(res) {
            console.log(res)
            console.log('save fail')
            wx.showModal({
              title: '提示',
              content: '保存失败，请刷新重试！',
              showCancel: false,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      },
      fail: function() {
        console.log('download fail')
        wx.showModal({
          title: '提示',
          content: '保存失败，请刷新重试！',
          showCancel: false,
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  callhim: function() {
    wx.showModal({
      title: '提示',
      content: '确定要拨打该电话吗？',
      success: function(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.makePhoneCall({
            phoneNumber: '18811593392'
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (getApp().globalData.settings.is_on_time_pay == '1') {
      wx.setNavigationBarTitle({
        title: '关于' + getApp().globalData.settings.company_name
      })
      this.setData({
        is_show: false
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  }
})