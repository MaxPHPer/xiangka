// pages/service/service.js
var util = require('../../utils/util.js');
var timestamp_to_minite = require('../../utils/util.js').timestamp_to_day;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    select_tab: 2,
    select_tab_for_service: 1,
    showLoading: 0,
    has_been_got: [],
    remained_num: [],
    switch_tab: [],
    vouchers_list: [],
    voucher_has_been_got: [],
    things_list: [{
      "id": "1",
      "name": "白银卡",
      "original_price": "100",
      "actual_price": "0.09",
      "times": "3",
      "photo_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
      "valid_start_timestamp": "1517414400",
      "valid_end_timestamp": "1556640000",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "shop_id": "1",
      "company_id": "1",
      "applied_fields": " A级护理 T级护理"
    }],

    // 周子皓 coupon start
    coupons_list: [{
      "id": "8",
      "name": "小皮筋",
      "original_price": "100",
      "actual_price": "50",
      "description": "",
      "photo_url": null,
      "discount": "0.65",
      "valid_start_timestamp": "1530720720",
      "valid_end_timestamp": "1531023120",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "total_num": "100",
      "consumed_num": "0",
      "allow_be_got_start_timestamp": "1528190880",
      "allow_be_got_end_timestamp": "1528450260",
      "each_person_may_receive": "10",
      "is_show": "1",
      "at_least_consume_money": "20",
      "allow_be_used_times": "10",
      "must_at_least_cost_money": "10",
      "is_just_allow_new_customer": "1",
      "state": "1",
      "shop_id": "1",
      "company_id": "1",
      "create_timestamp": "1528024356",
      "user_id": null,
      "is_has_been_got": 0
    }],
    display_discount: 9,
    coupon_text: "折扣券",

    // 周子皓 coupon end

    //yidian the last part

    good_category: [],
    good_by_category: [],

    //yidian the last part
  },

  //yidian

  shopcar: function() {
    wx.navigateTo({
      url: '../cart/cart',
    })
  },

  godetail: function(e) {
    var that = this;
    console.log('good_id:', e.currentTarget.id)
    wx.navigateTo({
      url: '../product_detail/product_detail?id=' + e.currentTarget.id,
    })
  },

  changetip: function(e) {
    var that = this;

    console.log('category_id:', e.currentTarget.id)
    //获得所有商品分类
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_good_by_category',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        good_category_id: e.currentTarget.id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('获得所有商品分类', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            good_by_category: res.data.data.good_by_category,
          })
          //console.log(that.data.good_by_category)
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  //yidan


  get_info: function(e) { //折扣券的详细信息
    var that = this;
    const idx = e.currentTarget.dataset.idx;

    var switch_tab_temp = that.data.switch_tab;
    if (switch_tab_temp[idx] == 1) {
      switch_tab_temp[idx] = 0
    } else switch_tab_temp[idx] = 1;
    that.setData({
      switch_tab: switch_tab_temp,
    })
  },

  choose: function(e) {
    var thing = e.currentTarget.dataset.thing
    // console.log(e.currentTarget)
    //getApp().globalData.du_choose = thing

    wx.navigateTo({
      url: '../detail/detail?name=' + thing.name + '&price=' + thing.price + '&mark=' + thing.mark + '&detail=' + thing.detail + '&original=' + thing.original_price + '&id=' + thing.id + '&source=' + 0
    })
  },

  get_cards_list: function() {
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Card/get_shop_allcard",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log(res)
        if (res.data.error_no == 0) {
          that.setData({
            things_list: res.data.data.times_cards
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取套餐列表失败',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取套餐列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },

  about: function() {
    wx.navigateTo({
      url: '../about/about'
    })
  },

  first_select_for_service: function() {
    this.get_cards_list()

    this.setData({
      select_tab_for_service: 1
    })
  },

  second_select_for_service: function() {
    this.setData({
      select_tab_for_service: 2
    })
  },

  third_select_for_service: function() {
    this.setData({
      select_tab_for_service: 3
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    this.get_cards_list()
    this.get_discount_list()
    this.get_all_cash()

    //yidian

    //获得所有商品分类
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_all_good_category',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('获得所有商品分类', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            good_category: res.data.data.all_good_category,
          })
          //console.log(that.data.good_category)
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })

    //获得所有商品分类的所有商品
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_good_by_category',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        good_category_id: -1,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('获得所有商品分类的所有商品', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            good_by_category: res.data.data.good_by_category,
          })
          //console.log(that.data.good_by_category)
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })

    //yidian
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.get_discount_list()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  first_select: function() {
    if (getApp().globalData.settings.version == "1") {
      wx.redirectTo({
        url: '../index/index'
      })
    } else if (getApp().globalData.settings.version == "2") {
      wx.redirectTo({
        url: '../index/index'
      })
    } else if (getApp().globalData.settings.version == "3") {
      wx.redirectTo({
        url: '../another_index/another_index'
      })
    }
  },

  third_select: function() {
    wx.redirectTo({
      url: '/pages/mine/mine'
    })
  },
  // 周子皓 coupon start
  get_discount_list: function() { //获得所有优惠券信息
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Card/get_all_discount_card",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          var remained_num = []
          var has_been_got_temp = []
          for (var k = 0; k < res1.data.data.discount_cards.length; k++) {
            has_been_got_temp.push(res1.data.data.discount_cards[k].is_has_been_got)
            remained_num.push(res1.data.data.discount_cards[k].remained_num)
          }
          var coupon_list_temp = res1.data.data.discount_cards
          for (var j = 0; j < res1.data.data.discount_cards.length; j++) {
            if (coupon_list_temp[j].is_fixed_valid_days == "0") {
              coupon_list_temp[j].valid_start_timestamp = timestamp_to_minite(parseInt(coupon_list_temp[j].valid_start_timestamp))
              coupon_list_temp[j].valid_end_timestamp = timestamp_to_minite(parseInt(coupon_list_temp[j].valid_end_timestamp))
            }
          }

          that.setData({
            // coupons_list: res1.data.data.discount_cards,
            coupons_list: coupon_list_temp,
            has_been_got: has_been_got_temp,
            remained_num: remained_num
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取优惠券列表失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },
  get_discount: function(e) { //获得优惠券
    var that = this;
    const idx = e.currentTarget.dataset.idx;

    var index = e.currentTarget.dataset.index
    var has_been_got_temp = that.data.has_been_got
    var remained_num = that.data.remained_num
    has_been_got_temp[index] = 1
    remained_num[index] = remained_num[index] - 1
    // console.log(111,idx);
    wx.request({
      url: getApp().globalData.server + "/API/Card/receive_discount_card",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id,
        username: getApp().globalData.userInfo_detail.username,
        card_id: idx
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          that.setData({
            has_been_got: has_been_got_temp,
            remained_num: remained_num
          })
          wx.showModal({
            title: '领取成功',
            content: '可以在在线买单中使用哦~',
            success: function(res2) {
              if (res2.confirm) {
                console.log('用户点击确定')
              } else if (res2.cancel) {
                console.log('用户点击取消')
              }
            }
          })
          // console.log(res1.data.data.discount_cards.discount)
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '领取失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },
  // 周子皓 coupon end
  //子皓 折扣券 开始
  get_all_cash: function() { //获得所有折扣券信息
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Cash/get_all_cash",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        if (res1.data.error_no == 0) {
          console.log("折扣券信息", res1)
          var has_been_got_temp = []
          for (var k = 0; k < res1.data.data.cashes.length; k++) {
            has_been_got_temp.push(res1.data.data.cashes[k].is_has_been_got)
          }
          var vouchers_list_temp = res1.data.data.cashes
          for (var j = 0; j < res1.data.data.cashes.length; j++) {
            if (vouchers_list_temp[j].is_fixed_valid_days == "0") {
              vouchers_list_temp[j].valid_start_timestamp = timestamp_to_minite(parseInt(vouchers_list_temp[j].valid_start_timestamp))
              vouchers_list_temp[j].valid_end_timestamp = timestamp_to_minite(parseInt(vouchers_list_temp[j].valid_end_timestamp))
            }
          }
          that.setData({
            voucher_has_been_got: has_been_got_temp,
            vouchers_list: vouchers_list_temp,

          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取优惠券列表失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },

  receive_cash: function(e) { //点击获得折扣券
    var that = this;
    const voucherid = e.currentTarget.dataset.voucherid;
    console.log(voucherid)
    // var index = e.currentTarget.dataset.index
    // var has_been_got_temp = that.data.has_been_got
    // var remained_num = that.data.remained_num
    // has_been_got_temp[index] = 1
    // remained_num[index] = remained_num[index] - 1
    // console.log(111,idx);
    wx.request({
      url: getApp().globalData.server + "/API/Cash/receive_cash",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id,
        username: getApp().globalData.userInfo_detail.username,
        voucher_id: voucherid
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          // that.setData({
          //   has_been_got: has_been_got_temp,
          //   remained_num: remained_num
          // })
          wx.showModal({
            title: '领取成功',
            content: '可以在在线买单中使用哦~',
            success: function(res2) {
              if (res2.confirm) {
                console.log('用户点击确定')
              } else if (res2.cancel) {
                console.log('用户点击取消')
              }
            }
          })
          // console.log(res1.data.data.discount_cards.discount)
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '领取失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取折扣券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
    that.get_discount_list()
  },


})