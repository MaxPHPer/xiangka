// pages/receive/receive.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    name: "",
    shop: "",
    status: "",
    source: "",
    order_id: "",
    disabled: 1
  },

  receive: function () {
    var that = this

    wx.showModal({
      title: '提示！',
      content: '确认收货吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')

          //将指定订单确认为已签收
          wx.request({
            url: getApp().globalData.server + '/API/Goods/comfirm_order_received',
            data: {
              shop_id: getApp().globalData.settings.shop_id,
              company_id: getApp().globalData.settings.company_id,
              user_id: getApp().globalData.userInfo_detail.user_id,
              order_id: that.data.order_id
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              console.log('将指定订单确认为已签收:', res.data)
              if (res.data.error_no != 0) {
                wx.showModal({
                  title: '哎呀～',
                  content: '出错了呢！' + res.data.data.error_msg,
                  success: function (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              }
              else if (res.data.error_no == 0) {
                that.setData({
                  status: "已签收",
                  disabled: 1
                })

              }
            },
            fail: function (res) {
              wx.showModal({
                title: '哎呀～',
                content: '网络不在状态呢！',
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var order_id = options.order_id
    that.setData({
      order_id: order_id
    })

    console.log("order_id", order_id)
    //得到指定的未使用的商品订单详情
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_one_unused_goods_order_detail',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        order_id: order_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('得到指定的未使用的商品订单详情', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          var data = res.data.data

          var status = ""
          var disabled = 1

          if (data.is_received == "1") {
            status = "确认已领取收货"
            disabled = 0
          } else if (data.is_received == "2") {
            status = "已签收"
            disabled = 1
          }

          that.setData({
            name: data.order_detail[0].goods_name,
            shop: getApp().globalData.settings.company_name + data.shop_name,
            source: data.note,
            status : status,
            disabled: disabled
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})