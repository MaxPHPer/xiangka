// pages/getScore/getScore.js

var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    total_score: 0,
    tasks: ["手机端签到", "邀请好友", "积分抽奖"],
    picture: ['/images/sign.png', '/images/share.png', '/images/bonus.png'],
    task_score: [],
    points_rules: {}
  },

  toTask: function (e) {
    var index = e.currentTarget.dataset.index

    if (index == 0) {
      wx.navigateTo({
        url: '../clock/clock',
      })
    }else if (index == 1){
      wx.navigateTo({
        url: '../sharing/sharing',
      })
    } else if (index == 2) {
      wx.navigateTo({
        url: '../lottery/lottery',
      })
    }

  },

  phone_signup:function(){
    if (getApp().globalData.userInfo_detail.is_phone_login == 0) {
      wx.navigateTo({
        url: '/pages/login/login'
      })
    }
    else if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      wx.showModal({
        title: '提示',
        content: '您已经登录！',
        showCancel: false,
        success: function (res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },

  charge:function(){
    wx.navigateTo({
      url: '/pages/recharge/recharge'
    })
  },

  buy_online:function(){
    wx.navigateTo({
      url: '/pages/paying/paying'
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    that.setData({
      total_score: getApp().globalData.userInfo_detail.point
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this
    //获取积分任务
    wx.request({
      url: getApp().globalData.server + "/API/Point/get_point_tasks",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res2) {
        console.log(res2)
        if (res2.data.error_no != 0) {
          wx.showModal({
            title: '哎呀',
            content: '获取积分任务失败' + res2.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res2.data.error_no == 0) {
          var task_score_temp = that.data.task_score
          task_score_temp.push(res2.data.data.points_rules.day_sign + " 积分")
          task_score_temp.push(res2.data.data.points_rules.invite_friend + " 积分")
          task_score_temp.push(res2.data.data.points_rules.lottery )

          that.setData({
            task_score: task_score_temp,
            points_rules: res2.data.data.points_rules
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取积分任务失败',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})