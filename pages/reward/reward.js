var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    datalist: [
      // {
      //   day: "1",
      //   goods: [{
      //     icon: '/images/jifen.png',
      //     id: "12",
      //     need_days: "1",
      //     to_add_goods_type: "1",
      //     name: "精品刷鞋",
      //     remainder: 423423,
      //     is_has_been_got: -1,
      //     botton_text: "还差1天"
      //   }],
      // },
      // {
      //   day: "3",
      //   goods: [{
      //     icon: '/images/huiyuanka.png',
      //     id: "6",
      //     need_days: "3",
      //     to_add_goods_type: "1",
      //     name: "T级护理",
      //     remainder: 2000,
      //     is_has_been_got: 0,
      //     botton_text: "还差3天"
      //   }, {
      //     icon: '/images/cup.png',
      //     id: "10",
      //     need_days: "3",
      //     to_add_goods_type: "0",
      //     name: "签到奖励4积分",
      //     remainder: 5000,
      //     is_has_been_got: -1,
      //     botton_text: "还差3天"
      //   }]
      // }
    ],
    guize: '  一 签到周期：周一00:00:00至周日00:00:00为一个签到周期，一周内用户累计签满三天和七天均可领取奖品。\n  二 领奖时间：领奖时间开始为第三天和第七天的10:00:00，领奖结束时间为第七天的23:59:59 '
  },

  formSubmit: function (e) {
    var that = this;

    wx.showModal({
      title: '提示！',
      content: '确定要领取该奖品吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('用户点击确定')
          const idx = e.detail.value.idx;
          console.log('奖品id：',idx);

          //确认领取连续签到的奖品
          wx.request({
            url: getApp().globalData.server + '/API/Sign/confirm_award_prize',
            data: {
              shop_id: getApp().globalData.settings.shop_id,
              company_id: getApp().globalData.settings.company_id,
              user_id: getApp().globalData.userInfo_detail.user_id,
              shop_name: getApp().globalData.settings.shop_name,
              username: getApp().globalData.userInfo_detail.username,
              id: idx,
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res) {
              console.log('确认领取连续签到的奖品', res.data)
              if (res.data.error_no != 0) {
                wx.showModal({
                  title: '哎呀～',
                  content: '出错了呢！' + res.data.data.error_msg,
                  success: function (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              }
              else if (res.data.error_no == 0) {
                //积分变动服务提醒
                if (res.data.data.to_add_goods_type == 0) {
                  console.log('积分变动微信提醒(formId)：', e.detail.formId)
                  //积分变动微信提醒
                  wx.request({
                    url: getApp().globalData.server + '/API/Point/point_wechat_notify',
                    data: {
                      company_id: getApp().globalData.settings.company_id,
                      shop_id: getApp().globalData.settings.shop_id,
                      user_id: getApp().globalData.userInfo_detail.user_id,
                      username: getApp().globalData.userInfo_detail.username,
                      openid: getApp().globalData.open_id,
                      form_id: e.detail.formId,
                      points_type: '新增',
                      points_from: '领取签到奖励',
                      consume_points: res.data.data.has_change_point,
                      total_points: res.data.data.current_point,
                    },
                    method: "POST",
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    success: function (res) {
                      console.log('积分变动微信提醒', res.data)
                      if (res.data.error_no != 0) {
                        // wx.showModal({
                        //   title: '哎呀～',
                        //   content: '出错了呢！' + res.data.data.error_msg,
                        //   success: function (res) {
                        //     if (res.confirm) {
                        //       console.log('用户点击确定')
                        //     } else if (res.cancel) {
                        //       console.log('用户点击取消')
                        //     }
                        //   }
                        // })
                      } else if (res.data.error_no == 0) { }
                    },
                    fail: function (res) {
                      // wx.showModal({
                      //   title: '哎呀～',
                      //   content: '网络不在状态呢！',
                      //   success: function (res) {
                      //     if (res.confirm) {
                      //       console.log('用户点击确定')
                      //     } else if (res.cancel) {
                      //       console.log('用户点击取消')
                      //     }
                      //   }
                      // })
                    }
                  })
                }

                //领取成功弹框
                wx.showModal({
                  title: "领取成功",
                  content: res.data.data.tip,
                  showCancel: false,
                  success: function (res2) {
                    if (res2.confirm) {
                      console.log('用户点击确定')
                      if (res.data.data.to_add_goods_type == 1) {
                        wx.redirectTo({
                          url: '../myorder/myorder?tab=1'
                        })
                      }
                      else if (res.data.data.to_add_goods_type == 3) {
                        getApp().globalData.userInfo_detail.second_balance = res.data.data.second_balance,
                          wx.reLaunch({
                            url: '../mine/mine'
                          })
                      }
                      else if (res.data.data.to_add_goods_type == 4) {
                        wx.redirectTo({
                          url: '../my_list/my_list'
                        })
                      }
                      else {
                        wx.redirectTo({
                          url: '../reward/reward'
                        })
                      }
                    } else if (res2.cancel) {
                      console.log('用户点击取消')
                    }
                  },
                })
              }
            },
            fail: function (res) {
              wx.showModal({
                title: '哎呀～',
                content: '网络不在状态呢！',
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获得连续签到奖励规则
    wx.request({
      url: getApp().globalData.server + '/API/Sign/get_all_continuous_sign_rules',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('获得连续签到奖励规则', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          that.setData({
            guize: res.data.data.rules
          })
          var prizes = res.data.data.prizes
          var keys = Object.keys(prizes)
          var list = []
          for (var i = 0; i < keys.length; i++) {
            var temp = {}
            temp.day = keys[i]
            temp.goods = prizes[keys[i]]
            for (var j = 0; j < temp.goods.length; j++) {
              if (temp.goods[j].to_add_goods_type == 0 || temp.goods[j].to_add_goods_type == 1 || temp.goods[j].to_add_goods_type == 3) {
                temp.goods[j].icon = '/images/jifen.png'
              }
              else if (temp.goods[j].to_add_goods_type == 2 || temp.goods[j].to_add_goods_type == 4) {
                temp.goods[j].icon = '/images/huiyuanka.png'
              }
              else {
                temp.goods[j].icon = '/images/jifen.png'
              }
            }
            list.push(temp)
          }
          that.setData({
            datalist: list
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})