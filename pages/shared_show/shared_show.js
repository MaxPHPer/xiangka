// pages/shared_show/shared_show.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    sharer_username: '',
    sharer_add_point: '0',
    add_point: '0',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("页面跳转传递", options)

    var that = this

    //打开分享增加积分
    wx.request({
      url: getApp().globalData.server + '/API/Point/open_share_successfully',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        sharer_id: options.share_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('打开分享增加积分', res.data)
        if (res.data.error_no == 10005) {
          wx.showModal({
            title: '提示',
            showCancel: false,
            content: '你已被邀请过了，不能被重复邀请哦！',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            },
            complete: function (res) {
              util.returntoIndex();
            }
          })
        } else if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            sharer_username: res.data.data.sharer_username,
            sharer_add_point: res.data.data.sharer_add_point,
            add_point: res.data.data.add_point,
          })
          getApp().globalData.userInfo_detail.point = res.data.data.current_point
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  },

  formSubmit: function (e) {
    var that = this
    console.log('积分变动微信提醒(formId)：', e.detail.formId)
    //积分变动微信提醒
    wx.request({
      url: getApp().globalData.server + '/API/Point/point_wechat_notify',
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        username: getApp().globalData.userInfo_detail.username,
        openid: getApp().globalData.open_id,
        form_id: e.detail.formId,
        points_type: '新增',
        points_from: '邀请好友',
        consume_points: that.data.add_point,
        total_points: getApp().globalData.userInfo_detail.point,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('积分变动微信提醒', res.data)
        if (res.data.error_no != 0) {
          // wx.showModal({
          //   title: '哎呀～',
          //   content: '出错了呢！' + res.data.data.error_msg,
          //   success: function (res) {
          //     if (res.confirm) {
          //       console.log('用户点击确定')
          //     } else if (res.cancel) {
          //       console.log('用户点击取消')
          //     }
          //   }
          // })
        } else if (res.data.error_no == 0) { }
      },
      fail: function (res) {
        // wx.showModal({
        //   title: '哎呀～',
        //   content: '网络不在状态呢！',
        //   success: function (res) {
        //     if (res.confirm) {
        //       console.log('用户点击确定')
        //     } else if (res.cancel) {
        //       console.log('用户点击取消')
        //     }
        //   }
        // })
      }
    })
    util.returntoIndex();
  },
})