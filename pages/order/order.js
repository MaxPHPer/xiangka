// pages/order/order.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    windowHeight: getApp().globalData.windowHeight,
    windowWidth: getApp().globalData.windowWidth,
    detailHeight: 0,
    buttonHeight: 0,
    quantity: 1,
    shop_name: "",
    thing: {
      "id": "1",
      "type": "times_card",
      "name": "白银卡",
      "original_price": "100",
      "actual_price": "0.09",
      "times": "3",
      "description": "<p>随意打的一段话<\/p>",
      "photo_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
      "valid_start_timestamp": "1517414400",
      "valid_end_timestamp": "1556640000",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "shop_id": "1",
      "company_id": "1",
      "create_timestamp": "1521950326",
      "goods": [{
        "id": "1",
        "name": "A级护理",
        "type": "1",
        "brief_description": "超级棒",
        "unit_price": "0.88",
        "original_price": "1.01",
        "discount": "0",
        "img_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
        "category_id": "2",
        "create_timestamp": "1521790402",
        "shop_id": "1",
        "company_id": "1"
      }, {
        "id": "2",
        "name": "T级护理",
        "type": "1",
        "brief_description": "超级好吃超市好吃",
        "unit_price": "0.33",
        "original_price": "100",
        "discount": "0",
        "img_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
        "category_id": "1",
        "create_timestamp": "1521790433",
        "shop_id": "1",
        "company_id": "1"
      }]
    },
    name: 18911206307,
    phone: 18911206307,
    userInfo_detail: {}
  },

  buy: function () {
    var that = this
    // console.log(getApp().globalData.userInfo)  
    wx.login({
      success: function (res) {
        if (res.code) {
          console.log(res.code)

          var temp_order_type = 0
          var temp_note = ""
          if (that.data.thing.type == "times_card") {
            temp_order_type = 2
            temp_note = "微信支付购买次卡"
          } else if (that.data.thing.type == "discount_card") {
            temp_order_type = 4
            temp_note = "微信支付购买折扣卡"
          }

          var goods_arr_tmp = [
            {
              goods_type: temp_order_type,
              goods_id: that.data.thing.id,
              goods_name: that.data.thing.name,
              unit_price: that.data.thing.actual_price,
              original_price: that.data.thing.original_price,
              photo_url: that.data.thing.photo_url,
              discount: "0",
              quantity: that.data.quantity,
              note: temp_note,
              company_id: getApp().globalData.settings.company_id,
              shop_id: getApp().globalData.settings.shop_id
            }
          ]

          // console.log(_order_type)
          wx.request({
            url: getApp().globalData.server + '/API/Shopping/do_order',
            data: {
              shop_id: getApp().globalData.settings.shop_id,
              company_id: getApp().globalData.settings.company_id,
              shop_name: getApp().globalData.settings.shop_name,
              user_id: getApp().globalData.userInfo_detail.user_id,
              username: getApp().globalData.userInfo_detail.username,
              note: temp_note,
              order_type: temp_order_type,
              goods_arr: JSON.stringify(goods_arr_tmp),
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (res1) {
              console.log(res1)
              if (res1.data.error_no != 0) {
                wx.showModal({
                  title: '哎呀～',
                  content: '创建订单失败！',
                  success: function (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              }
              else if (res1.data.error_no == 0) {
                wx.request({
                  url: getApp().globalData.server + "/API/Pay/pay_request",
                  data: {
                    code: res.code,
                    company_id: getApp().globalData.settings.company_id,
                    shop_id: getApp().globalData.settings.shop_id,
                    order_id: res1.data.data.order_id,
                    total_to_pay_money: res1.data.data.total_to_pay_money,
                    user_id: getApp().globalData.userInfo_detail.user_id
                  },
                  method: "POST",
                  header: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  success: function (res2) {
                    console.log(res2)
                    if (res2.data.error_no != 0) {
                      wx.showModal({
                        title: '哎呀',
                        content: '请求支付失败！' + res2.data.data.error_msg,
                        success: function (res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                    else if (res2.data.error_no == 0) {
                      var pay = require("../pay/pay.js").pay
                      pay(res2, that, 1)
                    }
                  },
                  fail: function (res) {
                    wx.showModal({
                      title: '哎呀～',
                      content: '请求支付失败！',
                      success: function (res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        } else if (res.cancel) {
                          console.log('用户点击取消')
                        }
                      }
                    })
                  }
                })
              }
            },
            fail: function (res) {
              wx.showModal({
                title: '哎呀～',
                content: '网络不在状态呢！',
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })


        } else {
          console.log('登录失败！' + res.errMsg)
          wx.showModal({
            title: '哎呀～',
            content: '内部错误1',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      }
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(getApp().globalData)
    this.setData({
      detailHeight: this.data.windowHeight * 0.92,
      buttonHeight: this.data.windowHeight * 0.08,
      thing: getApp().globalData.du_choose,
      userInfo_detail: getApp().globalData.userInfo_detail,
      shop_name: getApp().globalData.settings.shop_name
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})