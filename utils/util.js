const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

const currentDate = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join('-')
}

var GetCharLength = function(str) {
  ///<summary>获得字符串实际长度，中文2，英文1</summary>
  ///<param name="str">要获得长度的字符串</param>
  var realLength = 0,
    len = str.length,
    charCode = -1;
  for (var i = 0; i < len; i++) {
    charCode = str.charCodeAt(i);
    if (charCode >= 0 && charCode <= 128) realLength += 1;
    else realLength += 2;
  }
  return realLength;
}

//用户点击右上角分享
function share() {
  return {
    title: getApp().globalData.userInfo.nickName + '分享给你了一张会员卡！',
    path: '/pages/load/load',
    success: function(res) {
      // 转发成功
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '分享成功！',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        }
      })
    },
    fail: function(res) {
      // 转发失败
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '分享失败！',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          }
        }
      })
    }
  }
}

//跳转回主界面
function returntoIndex() {
  if (getApp().globalData.settings.version == "1") {
    //单店精英版
    wx.redirectTo({
      url: '../index/index'
    })
  } else if (getApp().globalData.settings.version == "2") {
    //多店连锁版
    wx.redirectTo({
      url: '../index/index'
    })
  } else if (getApp().globalData.settings.version == "3") {
    //商圈高级版
    wx.redirectTo({
      url: '../another_index/another_index'
    })
  }
}

function timestamp_to_minite(time_stamp) {
  // 比如需要这样的格式 yyyy-MM-dd hh:mm:ss
  var date = new Date(time_stamp * 1000);
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
  var D = date.getDate() + ' ';
  var h = date.getHours() + ':';
  var m = date.getMinutes();
  // var s = date.getSeconds();
  return Y + M + D + h + m; //呀麻碟
  // 输出结果：2014-04-23 18:55
}

/**
 * 将10位时间戳转为2014-04-23
 */
function timestamp_to_day(time_stamp) {
  // 比如需要这样的格式 yyyy-MM-dd hh:mm:ss
  var date = new Date(time_stamp * 1000);
  var Y = date.getFullYear() + '-';
  var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
  var D = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
  // var h = date.getHours() + ':';
  // var m = date.getMinutes();
  // var s = date.getSeconds();
  return Y + M + D; //呀麻碟
  // 输出结果：2014-04-23 
}

function parseQueryString(url) {
  var str = url.split("?")[1];
  var iterms = str.split("&");
  var arr, Json = {};
  for (var i = 0; i < iterms.length; i++) {
    arr = iterms[i].split("=");
    Json[arr[0]] = arr[1];
  }
  return Json;
}

module.exports = {
  share: share,
  returntoIndex: returntoIndex,
  formatTime: formatTime,
  currentDate: currentDate,
  GetCharLength: GetCharLength,
  timestamp_to_minite: timestamp_to_minite,
  timestamp_to_day: timestamp_to_day,
  parseQueryString: parseQueryString
}